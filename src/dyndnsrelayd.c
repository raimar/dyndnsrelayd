/**
 * A simple tcp server, which listens on incoming dyndns requests to delegate it to an other program.
 *
 * Here is an example:
 * <pre>
 * dyndnsrelayd 1111 6 10 "curl -# -n https://www.dnsdynamic.org/api/?hostname=my_own_dyndns_name.x64.me"
 * </pre>
 * The arguments are:
 * <ol>
 * <li>The port number for listening.</li>
 * <li>The maximum number of retries, if the result of the execution do not contain "good" or "nochg".</li>
 * <li>The number of second to wait for the next retry.</li>
 * <li>Command line, which will be executed after an incoming request containing "DDNS" has been arrived.</li>
 * </ol>
 * The command line from the example assumes the file ~/.netrc exists and contains domain name, user, and password, e.g.
 * <pre>
 * machine www.dnsdynamic.org login my_user_name password my_password
 * </pre>
 */
#include <stdbool.h> // bool in C
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h> // register for listening to CTRL-C and terminate signal

#define BUFFER_SIZE 256
#define MAX_RETRY 10
#define SECONDS_TO_WAIT_BEFORE_RETRY 10
int serverSocketHandle = -1;
int clientSocketHandle = -1;

/**
 * Close server socket and exit with 0 after program has been stoped with CTRL-C or terminate signal.
 */
void onSigIntSend(int parameter) {
	close(clientSocketHandle);
	close(serverSocketHandle);
	printf("Server stoped.\n", parameter);
	exit(0); // needed to exit program after signal
}

/**
 * Register for listening on CTRL+C and terminate signals.
 */
void registerSignals() {
	signal(SIGINT, onSigIntSend); // register for CTRL+C
	signal(SIGTERM, onSigIntSend); // register for terminate signal
}

/**
 * Print error message from main thread to standard error output and exit program.
 * @param msg The error message.
 */
void error(const char* msg) {
	fprintf(stderr, "Error: %s\n", msg);
	exit(1);
}

/**
 * Set process error message and exit the process.
 * @param msg The error message.
 */
void childError(const char* msg) {
	fprintf(stderr, "Error: %s\n", msg);
	perror(msg);
	exit(2);
}

/**
 * @param cmdLine The command to be executed.
 * @return {@code x>0} success after x retries,
 *         or {@code x<=0} for failure after -x retries.
 */
int executeCmdLine(const char* cmdLine, int maxRetries, int waitInSeconds) {
	int retry = 0;
	bool isRunning = true;
	while (isRunning && (retry < maxRetries)) {
		if (retry != 0) {
			printf("Wait %i seconds...\n", waitInSeconds);
			sleep(waitInSeconds); // wait a few seconds and then try again
		} else {
			printf("%s\n", cmdLine);
		}
		FILE* out = popen(cmdLine, "r"); // only reading is necessary for executing
		if (out == NULL) {
			printf("Failed to execute program.");
			isRunning = false; // stop loop, because starting command line failed
			retry = -retry;
		} else {
			char line[BUFFER_SIZE];
			if (fgets(line, sizeof(line) - 1, out) != NULL) {
				printf(line);
				if (strstr(line, "good") != NULL || strstr(line, "nochg") != NULL) {
					isRunning = false; // stop loop, because updating IP was successful
				}
			}
			fclose(out);
		}
		retry++;
	}
	if (isRunning) { // reached maximum of retry count
		retry = -retry;
	}
	return retry;
}

/**
 * 1. Check, if incoming connection contains "DDNS".
 * 2. Send "good" as answer.
 * 3. Execute program given as second parameter.
 * @param clientSocketHandle The handle for the client socket.
 * @param cmdLine The command line, that will be executed, if request contains "DDNS".
 */
void responseToRequest(int clientSocketHandle, char* cmdLine, int maxRetries, int waitInSeconds) {
	const char* const HTML_RESPONSE =
		"HTTP/1.1 200 OK\n"
		"Connection: close\n"
		"good\n";
	const char* const USER_AGENT_DDNS = "DDNS";

	printf("Reading from client...\n");
	char buffer[BUFFER_SIZE];
	int n = read(clientSocketHandle, buffer, sizeof(buffer) - 1); // read from client
	if (n < 0) {
		childError("Reading from client failed.");
	}
	if (strstr(buffer, USER_AGENT_DDNS) == NULL) {
		childError("Request not for DDNS.");
	}
	printf("%s\n", buffer);
	n = write(clientSocketHandle, HTML_RESPONSE, strlen(HTML_RESPONSE));
	if (n < 0) {
		childError("Writing to client failed.");
	}
	if (executeCmdLine(cmdLine, maxRetries, waitInSeconds) <= 0) {
		childError("Https request failed.");
	}
}

/**
 * Give listening port number as argument.
 * @param argc The number of arguments.
 * @param argv The array of arguments: Program name, listening port, and command line, e.g.
 * <pre>
 * dyndnsrelayd 1111 6 10 "curl -# -n https://www.dnsdynamic.org/api/?hostname=my.domain.name"
 * </pre>
 */
int main(int argc, char* argv[]) {
	if (argc < 5) {
		error("Pease give listening port number, and maximal retries, wait in seconds\nand command line as arguments, e.g.\n   dyndnsrelayd 1111 6 10 \"curl -# -n https://www.dnsdynamic.org/api/?hostname=my.domain.name\"");
	}
	registerSignals();
	 // execute cmd to ensure IP is updated directly after daemon has been started
	int maxRetries = atoi(argv[2]);
	int waitInSeconds = atoi(argv[3]);
	char* cmdLine = argv[4];
	if (executeCmdLine(cmdLine, maxRetries, waitInSeconds) <= 0) {
		exit(-3);
	}
	serverSocketHandle = socket(AF_INET, SOCK_STREAM, 0);
	if (serverSocketHandle < 0) {
		error("Creating socket failed.");
	}
	int listeningPort = atoi(argv[1]);
	struct sockaddr_in serverAddress;
	// bzero((char *) &serverAddress, sizeof(serverAddress)); // fill server addess struct with 0
	struct sockaddr clientAddress;
	serverAddress.sin_family      = AF_INET;
	serverAddress.sin_addr.s_addr = INADDR_ANY;
	serverAddress.sin_port        = htons(listeningPort);
	if (bind(serverSocketHandle, (struct sockaddr *) &serverAddress, sizeof(serverAddress)) < 0) {
		error("Opening listening port failed.");
	}
	listen(serverSocketHandle, 5);
	socklen_t clientLength = sizeof(clientAddress);
	while (true) {
		printf("Waiting for incoming connections (%i)...\n", getpid());
		clientSocketHandle = accept(serverSocketHandle, &clientAddress, &clientLength);
		if (clientSocketHandle < 0) {
			error("Accepting client failed.");
		}
		pid_t pid = 0;
		pid = fork(); // create new thread
		if (pid == 0) { // in new thread
			responseToRequest(clientSocketHandle, cmdLine, maxRetries, waitInSeconds);
			exit(0); // exit thread
		} else if (pid > 0) {
			waitpid(pid); // wait for child to prevent becoming a zombie
			close(clientSocketHandle); // socket needs to be closed here
		} else {
			error("main(): fork() failed.");
		}
	}
	return 0; // unreachable
}

